import Foundation
import Moya

protocol ListManagerType {
  func getAllListItems(success: @escaping ([ListItem]) -> Void, failure: @escaping () -> Void)
}

extension APIManager: ListManagerType {
  func getAllListItems(success: @escaping ([ListItem]) -> Void, failure: @escaping () -> Void) {
    provider.request(.getAllListItems) { (response) in
      switch response {
      case .success(let response):
        do {
          let successfullResponse = try response.filterSuccessfulStatusCodes()
          let listItemsResponse = try successfullResponse.map(ListItemsResponse.self)
          success(listItemsResponse.listItems)
        } catch {
          failure()
        }
      case .failure:
        failure()
      }
    }
  }
 }
