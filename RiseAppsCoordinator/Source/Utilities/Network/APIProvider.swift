import Foundation
import Moya

struct ServerConfig {
  static let baseUrl = "https://restcountries.eu/rest/v2"
}

enum APIProvider {
  case getAllListItems
}

extension APIProvider: TargetType {
  var baseURL: URL {
    guard let url = URL(string: ServerConfig.baseUrl) else { fatalError() }
    return url
    }

  var path: String {
    switch self {
    case .getAllListItems:
      return "/all"
    }
  }

  var method: Moya.Method {
    switch self {
    case .getAllListItems:
      return .get
    }
  }

  var task: Task {
    switch self {
    case .getAllListItems:
      return .requestPlain
    }
  }

  var sampleData: Data {
    return Data()
  }

  var headers: [String: String]? {
    var headers: [String: String] = ["Content-type": "application/json"]
    headers["X-Requested-With"] = "XMLHttpRequest"
    return headers
  }
}
