import Foundation

struct ListItemsResponse {
  var listItems: [ListItem]
}

extension ListItemsResponse: Decodable {
  init(from decoder: Decoder) throws {
    var values = try decoder.unkeyedContainer()
    listItems = []
    if let valuesCount = values.count {
      while values.currentIndex < valuesCount {
        listItems.append(try values.decode(ListItem.self))
      }
    }
  }
}
