import UIKit

enum Story: String {
  case main = "Main"
  case list = "List"
}

protocol Reusable {
  static var reuseIdentifier: String { get }
}

extension Reusable {
  static var reuseIdentifier: String {
    return String(describing: Self.self)
  }
}

protocol InterfaceBuilderPrototypable {
  static var nib: UINib { get }
}

extension InterfaceBuilderPrototypable {
  static var nib: UINib {
    return UINib(nibName: String(describing: Self.self), bundle: nil)
  }
}

extension UITableView {
  
  // MARK: - UITableViewCell
  
  func register<T: UITableViewCell>(_ : T.Type) where T: Reusable {
    register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
  }

  func register<T: UITableViewCell>(_ : T.Type) where T: Reusable, T: InterfaceBuilderPrototypable {
    register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
  }

  func dequeue<T: UITableViewCell>(_ : T.Type, for indexPath: IndexPath) -> T where T: Reusable {
    guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
      abort()
    }
    return cell
  }
}

protocol StoryboardIdentifiable {
  static var identifier: String { get }
}

protocol Presentable {
  var toPresent: UIViewController? { get }
}

extension UIStoryboard {
  convenience init(_ story: Story) {
    self.init(name: story.rawValue, bundle: nil)
  }

  func instantiateController<T: UIViewController>() -> T {
    guard let viewController = self.instantiateViewController(withIdentifier: T.identifier) as? T else {
      fatalError("Could not load view controller with identifier\(T.identifier)")
    }
    return viewController
  }
}

extension UIViewController: StoryboardIdentifiable {}
extension UIViewController: Presentable {
  var toPresent: UIViewController? {
    return self
  }
}

extension StoryboardIdentifiable where Self: UIViewController {
  static var identifier: String {
    return String(describing: self)
  }
}
