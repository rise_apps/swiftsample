import UIKit

class BaseViewController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
  override func viewDidLoad() {
    super.viewDidLoad()

    navigationController?.isNavigationBarHidden = true
  }

  deinit {
    print(String(describing: self))
  }

  override var prefersStatusBarHidden: Bool {
    return false
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .default
  }
}
