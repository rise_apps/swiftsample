import UIKit

protocol ListOutput: BaseViewType {
  var onSelectItem: ((ListItem) -> Void)! { get set }
  var listItems: [ListItem]! { get set }
}

class ListController: BaseViewController, ListOutput {
  
  // MARK: - Output
  
  var onSelectItem: ((ListItem) -> Void)!
  var listItems: [ListItem]!
  
  // MARK: - Private
  
  @IBOutlet private weak var tableView: UITableView!

  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureTableView()
  }

  // MARK: - Private
  private func configureTableView() {
    tableView.register(ListCell.self)
    tableView.delegate = self
    tableView.dataSource = self
  }
}

// MARK: - Table view

extension ListController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue(ListCell.self, for: indexPath)
    if indexPath.row < listItems.count {
      cell.configure(listItem: listItems[indexPath.row])
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return listItems.count
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row < listItems.count {
      onSelectItem(listItems[indexPath.row])
    }
  }
}
