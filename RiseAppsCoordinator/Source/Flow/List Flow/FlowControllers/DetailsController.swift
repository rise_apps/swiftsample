import UIKit

protocol DetailsOutput: BaseViewType, DismissableType {
  var itemDetails: ListItem! { get set }
}

class DetailsController: BaseViewController, DetailsOutput {
  
  // MARK: - Output
  
  var onDismiss: (() -> Void)?
  var itemDetails: ListItem!
  
  // MARK: - Private
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subTitleLabel: UILabel!
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
  }
  
  // MARK: - Actions
  
  @IBAction private func backButtonAction() {
    onDismiss?()
  }
  
  // MARK: - Private
  
  private func configureUI() {
    titleLabel.text = itemDetails.itemName
    subTitleLabel.text = "Capital: \(itemDetails.itemSubname)"
  }
}
