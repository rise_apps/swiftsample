import UIKit

final class ListCell: UITableViewCell, Reusable, InterfaceBuilderPrototypable {
  @IBOutlet weak var titleLabel: UILabel!
  
  func configure(listItem: ListItem) {
    titleLabel.text = listItem.itemName
  }
}
