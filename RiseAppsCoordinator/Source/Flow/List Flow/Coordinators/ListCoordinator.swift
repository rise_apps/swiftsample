import Foundation

protocol ListCoordinatorOutput: Coordinator {
  var finishFlow: (() -> Void)? { get set }
}

final class ListCoordinator: BaseCoordinator, ListCoordinatorOutput {
  
  // MARK: - Output
  
  var finishFlow: (() -> Void)?

  // MARK: - Property injection
  
  var listApiManager: ListManagerType!

  // MARK: - Init injection
  
  private let factory: ListModuleFactoryType
  private let router: RouterType

  // MARK: - Lifecycle
  
  init(factory: ListModuleFactoryType, router: RouterType) {
    self.factory = factory
    self.router = router
  }

  override func start() {
    showList()
  }
}

// MARK: - View

extension ListCoordinator {
  private func showList() {
    getAllListItems(success: { [unowned self](listItems) in
      DispatchQueue.main.async {
        let listOutput = self.factory.makeListOutput()
        listOutput.listItems = listItems
        listOutput.onSelectItem = { listItem in
          self.showDetails(item: listItem)
        }
        self.router.setRootModule(listOutput)
      }
    }, failure: {})
  }
  
  private func showDetails(item: ListItem) {
    let detailsOutput = factory.makeDetailsOutput()
    detailsOutput.itemDetails = item
    detailsOutput.onDismiss = { [unowned self] in
      self.router.popModule(animated: true)
    }
    router.push(detailsOutput, animated: true, completion: nil)
  }
}

// MARK: - API

extension ListCoordinator {
  private func getAllListItems(success: @escaping ([ListItem]) -> Void, failure: @escaping () -> Void) {
      let successHandler: ([ListItem]) -> Void = { listItems in
        success(listItems)
      }
      let failureHandler: () -> Void = {
        failure()
      }
      listApiManager.getAllListItems(success: successHandler, failure: failureHandler)
  }
}
