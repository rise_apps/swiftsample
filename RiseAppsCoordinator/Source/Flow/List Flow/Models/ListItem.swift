import Foundation

struct ListItem {
  let itemName: String
  let itemSubname: String
}

enum ListItemCodingKeys: String, CodingKey {
  case name
  case capital
}

extension ListItem: Decodable {
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: ListItemCodingKeys.self)
    itemName = try values.decode(String.self, forKey: .name)
    itemSubname = try values.decode(String.self, forKey: .capital)
  }
}
