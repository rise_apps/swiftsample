import Foundation

protocol ComponentFactoryType: class {
  func makeListAPIManager() -> ListManagerType
}

final class ComponentFactory: ComponentFactoryType {
  func makeListAPIManager() -> ListManagerType {
    return APIManager()
  }
}
