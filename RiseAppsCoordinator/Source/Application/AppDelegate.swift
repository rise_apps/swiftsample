import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  // MARK: - Lifecycle properties
  
  var window: UIWindow?

  // MARK: - Private properties
  
  private let coordinatorFactory = CoordinatorFactory()
  private lazy var applicationCoordinator: ApplicationCoordinator = self.makeCoordinator()()

  // MARK: - Lifecycle
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    configureWindow()
    return true
  }

  // MARK: - Private
  
  private func configureWindow() {
    window = UIWindow(frame: UIScreen.main.bounds)
    applicationCoordinator.start()
    window?.makeKeyAndVisible()
  }

  private func makeCoordinator() -> (() -> ApplicationCoordinator) {
    return { [unowned self] () -> ApplicationCoordinator in
      guard let window = self.window else { fatalError() }
      return self.coordinatorFactory.makeApplicationCoordinator(window: window)
    }
  }
}
