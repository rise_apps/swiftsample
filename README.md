# RiseAppsCoordinator
Based on the Andrey Panov Github [github.com](https://github.com/AndreyPanov/ApplicationCoordinator) .
You can read it for more details

I used a protocol for coordinators in this example:
```swift
protocol Coordinator: class {
  func start()
}
```
All flow controllers have a protocols (we need to configure blocks and handle callbacks in coordinators):
```swift
protocol Flow1Screen1Output: BaseViewType, DismissableType {
  var onNextScreenButtonPressed:(() -> Void)? { get set }
  var onSelectDate:((@escaping (String) -> Void) -> Void)? { get set }
  var onDateSelected:((String) -> Void)? { get set }
}
```
In this example I use factories for creating  coordinators and controllers (we can mock them in tests).
```swift
protocol CoordinatorFactoryType {
  func makeApplicationCoordinator(window: UIWindow) -> ApplicationCoordinator
  func makeTabBarOutput() -> TabbarViewControllerOutput
  func makeFlow1Coordinator() -> (Flow1CoordinatorOutput, Presentable)
  func makeFlow2Coordinator(_ navigationController: UINavigationController?) -> Flow2CoordinatorOutput
  func makeFlow3Coordinator(_ navigationController: UINavigationController?) -> Flow3CoordinatorOutput
}
```
The base coordinator stores dependencies of child coordinators
```swift
class BaseCoordinator: Coordinator {
  private var childCoordinators: [Coordinator] = []

  func start() {
    assertionFailure("must be overriden")
  }

  func addDependency(_ coordinator: Coordinator) {
    for element in childCoordinators where element === coordinator {
      return
    }

    childCoordinators.append(coordinator)
  }

  func removeDependency(_ coordinator: Coordinator?) {
    guard childCoordinators.isEmpty == false, let coordinator = coordinator else { return }

    for (index, element) in childCoordinators.enumerated() where element === coordinator {
      childCoordinators.remove(at: index)
      break
    }
  }

  func removeAllDependencies() {
    print("Must by removed: \(childCoordinators)")
    for coord in childCoordinators {
      removeDependency(coord)
    }
  }

  deinit {
    print(String(describing: self))
  }

  func dependencies() -> [Coordinator] {
    return childCoordinators
  }
}
```
AppDelegate store lazy reference for the Application Coordinator
```swift
class AppDelegate: UIResponder, UIApplicationDelegate {
  // MARK: - Lifecycle properties
  var window: UIWindow?

  // MARK: - Private properties
  private let coordinatorFactory = CoordinatorFactory()
  private lazy var applicationCoordinator: ApplicationCoordinator = self.makeCoordinator()()

  // MARK: - Lifecycle
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    configureWindow()
    return true
  }

  // MARK: - Private
  private func configureWindow() {
    window = UIWindow(frame: UIScreen.main.bounds)
    applicationCoordinator.start()
    window?.makeKeyAndVisible()
  }

  private func makeCoordinator() -> (() -> ApplicationCoordinator) {
    return { [unowned self] () -> ApplicationCoordinator in
      return self.coordinatorFactory.makeApplicationCoordinator(window: self.window!)
    }
  }
}
```

Also in this template we will use SwiftLint installed via homebrew, please install it if you don't have it [swiflint](https://github.com/realm/SwiftLint#using-homebrew), swiftlint config file is present in project folder and xcode project configured to use swiftlint.

Also in this template we have set of common pods, so before running project, please run pod install.
pod 'Fabric' - used for build distribution;
pod 'Crashlytics' - used for logging crashes, add correct configuration in Xcode>Build Phases>Crashlytics;
pod 'Moya' - used as API Manager, template have example api call, configure it according your backend requirements. Also use Swift 4 Codable protocol to decode/encode JSON with Moya methods;
pod 'Kingfisher' - used for uploading images;
pod 'IQKeyboardManagerSwift' - used for handling keyboard with better UX, configure it according your project needs;
